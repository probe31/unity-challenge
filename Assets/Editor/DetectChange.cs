﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DetectChange : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (string str in importedAssets)
        {
            DoReimport(str);
        }
    }

    public static bool IsChallengeJson(string path)
    {
        return path.Contains(Constants.JSONCHALLENGE_FILENAME) && path.EndsWith(".json", StringComparison.OrdinalIgnoreCase);
    }

    public static void DoReimport(string str)
    {
        if (!IsChallengeJson(str))
            return;

        if(EventsManager.Instance)
            EventsManager.Instance.CallOnJsonChange();
    }
}
