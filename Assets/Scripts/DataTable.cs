﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CellData
{
    public string value;
    public CellData(string _value)
    {
        this.value = _value;
    }
}

[Serializable]
public class RowData
{
    public List<CellData> cellData = new List<CellData>();

}

[Serializable]
public class DataTable 
{
    public string Title;
    public string[] ColumnHeaders;
    public List<RowData> Rows = new List<RowData>();
   
}
