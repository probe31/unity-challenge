﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager: MonoBehaviour
{
    public static EventsManager Instance;

    void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    public delegate void JsonChange();
    public static event JsonChange OnJsonChange;
    public void CallOnJsonChange()
    {
        OnJsonChange?.Invoke();
    }
}
