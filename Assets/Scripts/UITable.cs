﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;
using UnityEngine.UI;

public class UITable : MonoBehaviour
{
    public GridLayoutGroup gridLayout;
    const int defaultCellHeight = 100;
    List<GameObject> cells = new List<GameObject>();
    public GameObject headerPrefab;
    public GameObject cellPrefab;
    public Text titleText;
    void Start()
    {
        CreateTable(JsonReaderUnity.ReadJson(Constants.JSONCHALLENGE_FILENAME));
        EventsManager.OnJsonChange += ReloadTable;
    }

    private void OnDestroy()
    {
        EventsManager.OnJsonChange -= ReloadTable;
    }

    void CreateTable(DataTable dataTable)
    {
        titleText.text = dataTable.Title;
        int columns = dataTable.ColumnHeaders.Length;
        int rows = dataTable.Rows.Count;
        
        Vector2 cellSize = new Vector2(Constants.REFERENCE_WIDTH / columns, defaultCellHeight);
        gridLayout.cellSize = cellSize;

        for (int i = 0; i < columns; i++)
        {            
            CreateCell(headerPrefab, dataTable.ColumnHeaders[i]);
        }

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                CreateCell(cellPrefab, dataTable.Rows[i].cellData[j].value);
            }
        }

    }

    GameObject CreateCell(GameObject prefab, string value)
    {
        GameObject cell = GameObject.Instantiate(prefab);
        cell.transform.SetParent(gridLayout.transform);
        cell.GetComponent<UICell>().SetValue(value);
        cells.Add(cell);
        return cell;
    }

    public void ClearTable()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            GameObject.Destroy(cells[i]);
        }
    }

    public void ReloadTable()
    {
        ClearTable();
        CreateTable(JsonReaderUnity.ReadJson(Constants.JSONCHALLENGE_FILENAME));
    }
}
