﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public const string JSONCHALLENGE_FILENAME = "JsonChallenge";
    public const float REFERENCE_WIDTH = 1980;
}
