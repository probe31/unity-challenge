﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICell : MonoBehaviour
{
    public Text valueText;
    public void SetValue(string value)
    {
        valueText.text = value;
    }
}
