﻿using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public static class JsonReaderUnity
{
    public static DataTable ReadJson(string fileName)
    {
        string filePath = Application.dataPath + "/StreamingAssets/" + fileName + ".json";

        if (File.Exists(filePath)) 
        {
            string dataAsJson = File.ReadAllText(filePath);
            DataTable datatable = JsonConvert.DeserializeObject<DataTable>(dataAsJson, new CustomJsonConverter());
            return datatable;
        }
        else
        {
            Debug.LogError("Cannot find the file!");
            return null;
      }
    }
}
