﻿using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

public class CustomJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        //throw new NotImplementedException();
        return true;
    }

    public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        DataTable result = new DataTable();
        RowData rows = new RowData();

        if(reader.TokenType == JsonToken.StartObject)
        {
            JObject item = JObject.Load(reader);
            result.Title = item["Title"].Value<string>();
            result.ColumnHeaders = item["ColumnHeaders"].ToObject<string[]>();
            JToken[] tokens = item["Data"].ToArray();
            for (int i = 0; i < tokens.Length; i++)
            {
                RowData rowData = new RowData();
                
                for (int j = 0; j < result.ColumnHeaders.Length; j++)
                {
                    rowData.cellData.Add(new CellData(tokens[i].Value<string>(result.ColumnHeaders[j])));
                }

                result.Rows.Add(rowData);
            }

        }        
        return result;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}
